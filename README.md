# A Guix System skeleton configuration

This configuration should form the basis of a simple Guix System that can be built upon as desired.

The key files are:

* *system.scm*, which defines the system
* *home.scm*, which defines the user's home environment

## system.scm

This configuration assumes that
* the bootloader will be installed on /dev/sda. Change this to suit your bootdisk.
* the system root directory will mount an existing ext4 partition labeled "system". Again, adjust as required.
* there will be a single user *main* with the default users group and supplementary-groups necessary to use sudo and the internet (needed to update the system) and X. A different name can be used and the supplementary-groups added to at will.
* the only packages explicitly required for all users is *nss-certs* to access the internet and *dbus, sx and xhost* to start X. If you add to these, you may need to update `(use-modules ...)` and `(use-package-modules ...)`.
* the only services required are the basic ones *%base-services*, plus enough to log in to a console, access the internet and run X. Again, if you add to these, you may need to update `(use-modules ...)` and `(use-service-modules ...)`.
* the entries for host-name, locale, timezone and keyboard-layout are placeholders and can be changed to anything more suitable.

This system can be installed using `sudo guix system reconfigure system.scm`

```
     (use-modules (gnu) (guix packages))
     (use-package-modules glib certs xdisorg xorg)
     (use-service-modules avahi base dbus desktop networking xorg)

     (operating-system
       (host-name "len")
       (locale "en_US.utf8")
       (timezone "Europe/Madrid")
       (users
        (cons*
         (user-account
          (name "main")
          (comment "Main user account")
          (group "users")
          (supplementary-groups '("wheel" "netdev" "audio" "video" "input")))
         %base-user-accounts))
       (keyboard-layout
        (keyboard-layout "us" "dvorak" #:options '("ctrl:swapcaps_hyper" "compose:ralt")))
       (bootloader
        (bootloader-configuration
         (bootloader grub-bootloader)
         (targets '("/dev/sda"))
         (keyboard-layout keyboard-layout)))
       (file-systems
        (cons*
         (file-system
           (mount-point "/")
           (device (file-system-label "system"))
           (type "ext4"))
         %base-file-systems))
       (packages
        (cons* dbus nss-certs sx xhost %base-packages))
       (services
        (cons* 	    
         (service dhcp-client-service-type)
         (service avahi-service-type)
         (elogind-service)
         (dbus-service)
         (service xorg-server-service-type
                  (xorg-configuration
                   (keyboard-layout keyboard-layout)))
         %base-services)))
```

## home.scm

This home configuration can be installed using `guix home reconfigure home.scm`

```
(use-modules (gnu)
	     (gnu home)
	     (gnu home services)
	     (gnu home services shells)
	     (gnu services)
	     (guix gexp))

(home-environment
 (services (list
	    (service
	     home-bash-service-type
	     (home-bash-configuration
	      (guix-defaults? #t)
	      (environment-variables
	       ;; set bash history file location
	       '(("HISTFILE" . "~/.bash_history")))
	      ;; append extra-profiles to the guix generated ~/.bash-profile
	      (bash-profile (list (local-file "extra-profiles")))))
	    (simple-service
	     'home-config
	     home-files-service-type
	     (list
	      `(".config/emacs/init.el"
		,(local-file "emacs-init"))
	      ;; :recursive? #t for a file preserves file permissions, in this case
	      ;; the local file sx-config is executable
	      `(".config/sx/sxrc"
		,(local-file "sx-config" #:recursive? #t)))))))
```

### extra-profiles

Update PATH, MANPATH and INFOPATH for packages installed by profiles.

```
#packages installed with guix package by manifest as the next generation of a profile
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles

for i in $GUIX_EXTRA_PROFILES/*; do
    profile=$i/$(basename "$i")
    if [ -f "$profile"/etc/profile ]; then
        GUIX_PROFILE="$profile"
        . "$GUIX_PROFILE"/etc/profile
    fi
    unset profile
done
```

#### emacs-packages-manifest.scm

The emacs lisp packages needed by the emacs configuration.

```
(specifications->manifest
 (list
  "emacs-exwm"
  ;; to include info and man files in appropriate paths
  "info-reader"
  "man-db"))
```

Install with

```
mkdir -p ~/.guix-extra-profiles/emacs && \
guix package -m emacs-packages-manifest.scm -p ~/.guix-extra-profiles/emacs/emacs
```

#### emacs-support-packages-manifest.scm

Packages needed to support the emacs configuration.

```
(specifications->manifest
 (list
  "emacs"
  "wmctrl"
  ;; to include info and man files in appropriate paths
  "info-reader"
  "man-db"))
```

Install with:

```
mkdir -p ~/.guix-extra-profiles/support && \
guix package -m emacs-support-packages-manifest.scm -p ~/.guix-extra-profiles/support/support   
```

#### other-packages-manifest.scm

Other useful packages.

```
(specifications->manifest
 (list
  "icecat"
  ;; use in manifests to include infos and mans in appropriate paths
  "info-reader"
  "man-db"))
```
Install with:

```
mkdir -p ~/.guix-extra-profiles/other && \
guix package -m other-packages-manifest.scm -p ~/.guix-extra-profiles/other/other
```

### emacs-init

```
;; update load-path for emacs-lisp packages installed by Guix
(dolist (dir '(;; emacs packages installed for the 'emacs' extra profile
               "~/.guix-extra-profiles/emacs/emacs/share/emacs/site-lisp/"
               ;; emacs packages installed using `guix home' or `guix package'
               "~/.guix-home/profile/share/emacs/site-lisp/"))
  (let ((default-directory dir))
    (normal-top-level-add-subdirs-to-load-path)))


;; This init file will be immutable. Use a separate writable custom file.
(setq custom-file "~/.config/emacs/custom")
(load custom-file t)

;;;
;;; Insert your configuration here
;;;

;; wmctrl responds "Name:..." with name of running window manager
;; or "Cannot get window manager..." if there isn't one
(when (and (equal window-system 'x)
           (string= (substring (shell-command-to-string "wmctrl -m")
                               0 1)
                    "C"))
  (require 'exwm)
  (require 'exwm-config)

  ;; delete the following line and replace with your exwm configuration
  (exwm-config-example)
  ;;;
  ;;; Insert your exwm configuration here
  ;;;
  )
```

### sx-config

sx-config must be executable, so that .config/sx/sxrc will be

```
xhost +SI:localuser:$USER
exec dbus-run-session -- emacs
```
